﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using Apprenda.FileServices;

namespace Apprenda.ResourceEditor
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IFileService
    {
        [OperationContract]
        FileContext GetFiles(string subPath);

        [OperationContract]
        FileInformation GetFile(string path);

        [OperationContract]
        string EditFile(string file);

        [OperationContract]
        bool CancelEdits(string masterFolder);

        [OperationContract]
        bool SaveFile(string file, byte[] data);

        [OperationContract]
        bool DeleteFile(string file);

        [OperationContract]
        bool DeleteFiles(string[] files);

        [OperationContract]
        bool Initialize(InteractionContext context);

        [OperationContract]
        bool SaveEdits(InteractionContext context);

    }

}
