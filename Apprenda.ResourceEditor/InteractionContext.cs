﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Apprenda.ResourceEditor
{
    [DataContract, Serializable]
    public class InteractionContext
    {
        private ContextInfo _tenantInfo;
        private ContextInfo _applicationInfo;
        private ContextInfo _versionInfo;
        private Guid _interactionId;

        public InteractionContext()
        {
            _interactionId = Guid.NewGuid();
        }

        public InteractionContext(string tenantAlias,
            string tenantGuid,
            string appAlias,
            string appGuid,
            string verAlias,
            string verGuid)
        {
            _tenantInfo = new ContextInfo { Alias = tenantAlias, UniqueID = tenantGuid };
            _applicationInfo = new ContextInfo { Alias = appAlias, UniqueID = appGuid };
            _versionInfo = new ContextInfo { Alias = verAlias, UniqueID = verGuid };
            _interactionId = Guid.NewGuid();
        }

        public ContextInfo Tenant
        {
            get { return _tenantInfo; }
        }

        [DataMember]
        public string TenantAlias
        {
            get { return _tenantInfo.Alias; }
            set {  _tenantInfo.Alias = value; }
        }

        [DataMember]
        public string TenantGuid
        {
            get { return _tenantInfo.UniqueID; }
            set { _tenantInfo.UniqueID = value; }
        }

        public ContextInfo Application
        {
            get { return _applicationInfo; }

        }

        [DataMember]
        public string ApplicaitonAlias
        {
            get { return _applicationInfo.Alias; }
            set { _applicationInfo.Alias = value; }
        }

        [DataMember]
        public string ApplicationGuid
        {
            get { return _applicationInfo.UniqueID; }
            set { _applicationInfo.UniqueID = value; }
        }

        public ContextInfo Version
        {
            get { return _versionInfo; }
        }

        [DataMember]
        public string VersionAlias
        {
            get { return _versionInfo.Alias; }
            set { _versionInfo.Alias = value; }
        }

        [DataMember]
        public string VersionGuid
        {
            get { return _versionInfo.UniqueID; }
            set { _versionInfo.UniqueID = value; }
        }

        [DataMember]
        public Guid InteractionId
        {
            get { return _interactionId; }
            set { _interactionId = value; }
        }

        [DataMember]
        public string ApplicationDetails
        {
            get { return $@"\{TenantAlias}\{ApplicaitonAlias}\{VersionAlias}"; }
            private set { }
        }
    }

    [Serializable]
    public struct ContextInfo
    {
        public string Alias;
        public string UniqueID;
    }
}
