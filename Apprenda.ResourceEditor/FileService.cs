﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Configuration;
using Platform.Automation.Service;
using Apprenda.FileServices;
using Apprenda.SaaSGrid;
using Apprenda.Services.Logging;

namespace Apprenda.ResourceEditor
{
    // Using PerSession Instacing to allow for stateful commands for the user activity:
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerSession)]
    public class FileService : IFileService
    {

        public FileService()
        {
            // Initialze Service Session Contexts:

            // Get values to authenticate against the SOC - better to use AuthHeader values
            // than using Credentials -- Unless Service Credentials are created for the service.
            var platformUrl = ConfigurationManager.AppSettings["PlatformUrl"];
            var serviceHead = "";
            var servicePass = "";

            // If we are using Service Credentials for platform authN:
            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["ServiceUser"]))
            {
                serviceHead = ConfigurationManager.AppSettings["ServiceUser"];
                servicePass = ConfigurationManager.AppSettings["ServicePassword"];
            }
            // If we are using custom Auth Plugin for platform authN:
            else if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["AuthHeader"]))
            {
                serviceHead = ConfigurationManager.AppSettings["AuthHeader"];
                servicePass = ConfigurationManager.AppSettings["SharedSecret"];
            }
            var platformAuthHeader = ConfigurationManager.AppSettings["AuthHeader"];
            var platformSharedSecret = ConfigurationManager.AppSettings["SharedSecret"];

        }

        public bool DeleteFile(string file)
        {
            var filePath = $@"{ConfigurationManager.AppSettings["TemporaryDirectory"]}\{file}";

            return FileManager.DeleteFile(filePath);
        }

        public bool DeleteFiles(string[] files)
        {
            return FileManager.DeleteFiles(files);
        }

        public string EditFile(string file)
        {
            return FileManager.EditFileText(file);
        }

        public FileContext GetFiles(string path)
        {
            var temporaryDirectory = ConfigurationManager.AppSettings["TemporaryDirectory"];
            var filters = ConfigurationManager.AppSettings["FileFilter"];
            return FileManager.GetFiles($@"{temporaryDirectory}{path}", filters);
        }

        public bool Initialize(InteractionContext context)
        {
            if (context != null)
            {
                // Get the Platform Repository and Temporary Directory Locations:
                var platformRepoLocation = ConfigurationManager.AppSettings["PlatformRepository"];
                var temporaryDirectory = ConfigurationManager.AppSettings["TemporaryDirectory"];

                // Get the Sub Directory of the Application from the InteractionContext:
                platformRepoLocation += context.ApplicationDetails;
                temporaryDirectory += $@"{context.ApplicationDetails}\{context.InteractionId}";

                // Create a copy of the Application from the Repository:
                return FileManager.CreateCopy(platformRepoLocation, temporaryDirectory);
            }

            return false;
        }

        public bool SaveEdits(InteractionContext context)
        {
            if (context != null)
            {
                // Get the Platform Repository and Temporary Directory Locations:
                var platformRepoLocation = ConfigurationManager.AppSettings["PlatformRepository"];
                var temporaryDirectory = ConfigurationManager.AppSettings["TemporaryDirectory"];

                // Get the Sub Directory of the Application from the InteractionContext:
                platformRepoLocation += context.ApplicationDetails;
                temporaryDirectory += $@"{context.ApplicationDetails}\{context.InteractionId}\base";

                return FileManager.FinializeCopy(platformRepoLocation, temporaryDirectory);
            }

            return false;
        }

        public bool SaveFile(string file, byte[] data)
        {
            var filePath = $@"{ConfigurationManager.AppSettings["TemporaryDirectory"]}\{file}";
            return FileManager.SaveFile(file, data);
        }

        public FileInformation GetFile(string path)
        {
            var filePath = $@"{ConfigurationManager.AppSettings["TemporaryDirectory"]}\{path}";

            return FileManager.GetFile(filePath);
        }

        public bool CancelEdits(string masterFolder)
        {
            var filePath = $@"{ConfigurationManager.AppSettings["TemporaryDirectory"]}\{masterFolder}";

            return FileManager.CancelCopy(filePath);
        }
    }
}
