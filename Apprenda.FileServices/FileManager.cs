﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Compression;

namespace Apprenda.FileServices
{
    public static class FileManager
    {

        public static FileContext GetFiles(string path, string searchFilter = "", SearchOption option = SearchOption.TopDirectoryOnly)
        {
            var _directoryInfo = new DirectoryInfo(path);

            var tempContext = new FileContext();

            try
            {
                var filters = searchFilter.Split('|');

                if (filters.Length > 1)
                {
                    foreach (var filter in filters)
                    {
                        tempContext.Files.AddRange(_directoryInfo.EnumerateFiles($"*{filter}", option));
                    }
                }
                else
                {
                    tempContext.Files.AddRange(_directoryInfo.EnumerateFiles($"{searchFilter}", option));
                }

                tempContext.Directories.AddRange(_directoryInfo.EnumerateDirectories());
            }
            catch (Exception ex)
            {

            }

            // Sync the context:
            tempContext.SyncStubs();

            return tempContext;

        }

        public static bool CreateCopy(string repoLocation, string tempDirectory)
        {
            try
            {
                // Copy the contents of the app in the Repo to Temp location:
                Helper.DirectoryCopy(repoLocation, tempDirectory, true);

                // Unzip each archive:
                var context = GetFiles(tempDirectory, searchFilter: "*.zip", option: SearchOption.AllDirectories);

                foreach (var file in context.Files.Where(f => f.Extension == ".zip"))
                {
                    using (ZipArchive zip = ZipFile.Open(file.FullName, ZipArchiveMode.Read))
                    {
                        zip.ExtractToDirectory(file.DirectoryName);
                    }

                    // Remove existing Component.zip:
                    File.Delete(file.FullName);
                }

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static bool FinializeCopy(string repoLocation, string tempDirectory)
        {
            try
            {
                // Get Directories to check:
                var interfaces = $@"{tempDirectory}\Interfaces";
                var services = $@"{tempDirectory}\Services";
                var winServices = $@"{tempDirectory}\WinServices";

                // Zip new archives:
                ZipFolder(interfaces);
                ZipFolder(services);
                ZipFolder(winServices);

                // Replace the repo version:
                //Helper.DirectoryCopy(tempDirectory, repoLocation, true);

                // Delete the copy:
                Helper.DeleteCopy(tempDirectory.Remove(tempDirectory.LastIndexOf('\\')), true);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        public static bool CancelCopy(string directory)
        {
            try
            {
                Helper.DeleteCopy(directory, true);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public static void ZipFolder(string directory)
        {
            if (Directory.Exists(directory))
            {
                foreach (var dir in Directory.EnumerateDirectories(directory))
                {
                    var parent = Directory.GetParent(dir);

                    ZipFile.CreateFromDirectory(dir, $@"{parent}\component.zip");

                    Helper.DeleteCopy(dir, false);

                    File.Move($@"{parent}\component.zip", $@"{dir}\component.zip");
                }
            }
        }

        public static FileInformation GetFile(string file)
        {
            var info = new FileInfo(file);

            var result = new FileInformation
            {
                Name = info.Name,
                Extension = info.Extension,
                Content = File.ReadAllBytes(file)
            };

            return result;
        }

        public static string EditFileText(string file)
        {
            return File.ReadAllText(file);
        }

        public static bool SaveFile(string file, byte[] data)
        {
            try
            {
                File.WriteAllBytes(file, data);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public static bool DeleteFile(string file)
        {
            try
            {
                File.Delete(file);
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

        public static bool DeleteFiles(string[] files)
        {
            try
            {
                foreach (var f in files)
                {
                    File.Delete(f);
                }
            }
            catch (Exception e)
            {
                return false;
            }

            return true;
        }

    }
}
