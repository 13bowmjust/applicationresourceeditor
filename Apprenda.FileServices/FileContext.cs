﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Apprenda.FileServices
{
    [DataContract]
    public class FileContext
    {

        private List<FileInfo> _files;
        private List<DirectoryInfo> _directories;
        private List<FileInformation> _fileStubs;
        private List<DirectoryInformation> _dirStubs;

        public FileContext()
        {
            _files = new List<FileInfo>();
            _directories = new List<DirectoryInfo>();
            _fileStubs = new List<FileInformation>();
            _dirStubs = new List<DirectoryInformation>();
        }

        public FileContext(List<FileInfo> files, List<DirectoryInfo> directories)
        {
            _files = files;
            _directories = directories;
            _fileStubs = new List<FileInformation>();
            _dirStubs = new List<DirectoryInformation>();
            SyncStubs();
        }

        public List<FileInfo> Files
        {
            get { return _files; }
            set { _files = value; } 
        }

        public List<DirectoryInfo> Directories
        {
            get { return _directories; }
            set { _directories = value; }
        }

        [DataMember]
        public List<FileInformation> FileStubs
        {
            get { return _fileStubs; }
            set { _fileStubs = value; }
        }

        [DataMember]
        public List<DirectoryInformation> DirectoryStubs
        {
            get { return _dirStubs; }
            set { _dirStubs = value; }
        }

        public void SyncStubs()
        {
            foreach (var file in _files)
            {
                _fileStubs.Add(new FileInformation
                {
                    Name = file.Name,
                    FullName = file.FullName,
                    Extension = file.Extension
                });
            }

            foreach (var dir in _directories)
            {
                _dirStubs.Add(new DirectoryInformation
                {
                    Name = dir.Name,
                    FullName = dir.FullName,
                    Parent = dir.Parent.Name
                });
            }
        }

    }
}
