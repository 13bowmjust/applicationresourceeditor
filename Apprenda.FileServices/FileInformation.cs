﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Apprenda.FileServices
{
    [DataContract]
    public class FileInformation
    {

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string FullName { get; set; }

        [DataMember]
        public string Extension { get; set; }

        [DataMember]
        public byte[] Content { get; set; }


    }
}
