﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Apprenda.ResourceEditor.Api.Models;
using Apprenda.ResourceEditor.Api.ResourceEditorClient;
using Apprenda.SaaSGrid;

namespace Apprenda.ResourceEditor.Api.Controllers
{
    public class ServiceController : ApiController
    {
        // _serviceClient acts as the main channel for actions to the Resource
        // Editor WCF Service - All file actions run through here:
        private FileServiceClient _serviceClient = new FileServiceClient();

        // Logger is the reference to the platform logging operations:
        //private static ILogger logger = LogManager.Instance().GetLogger("Test Resource Editor");

        #region Constructor:

        public ServiceController()
        {
            if (GetContext<InteractionContext>("ResourceEditor-Context") == null)
            {
                // Get Headers from the Request:
                var headerValues = Request.Headers.GetValues("ResourceEditor").ToList();

                var context = new InteractionContext()
                {
                    TenantAlias = headerValues[0],
                    ApplicaitonAlias = headerValues[1],
                    VersionAlias = headerValues[2]
                };
                _serviceClient.Initialize(context);
                SetContext("ResourceEditor-Context", context);
                SetContext("ResourceEditor-Position", "base");
            }
        }

        #endregion

        #region Controller Methods:

        [ResponseType(typeof(FileContextDto))]
        [HttpPost]
        public IHttpActionResult GetData([FromUri]string path = "")
        {
            if (!string.IsNullOrEmpty(path))
            {
                SetPath(path);
            }

            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");

            var filePath = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}";

            var files = _serviceClient.GetFiles(filePath);

            var fileContextDto = new FileContextDto
            {
                Context = files,
                Position = currentPosition,
                Application = $"{context.ApplicaitonAlias} - {context.VersionAlias}",
                InteractionId = context.InteractionId.ToString()
            };

            return Ok(fileContextDto);
        }

        [ResponseType(typeof(string))]
        [HttpGet]
        public IHttpActionResult SaveData()
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");

            var result = _serviceClient.SaveEdits(context);

            if (result)
            {
                PurgeContext("ResourceEditor-Context");
                PurgeContext("ResourceEditor-Position");

                return Ok($"developer/app/#/apps/dashboard/{context.ApplicaitonAlias}/{context.VersionAlias}");
            }

            return BadRequest("Failed to Save Data from the edits.");
        }

        [ResponseType(typeof(string))]
        [HttpGet]
        public IHttpActionResult CancelData()
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var path = $@"{context.ApplicationDetails}\{context.InteractionId}";

            var result = _serviceClient.CancelEdits(path);

            if (result)
            {
                PurgeContext("ResourceEditor-Context");
                PurgeContext("ResourceEditor-Position");

                return Ok($"developer/app/#/apps/dashboard/{context.ApplicaitonAlias}/{context.VersionAlias}");
            }

            return BadRequest("Failed to Cancel Data from the edits.");
        }

        [ResponseType(typeof(string))]
        [HttpPost]
        public IHttpActionResult UploadFile(HttpPostedFileBase file)
        {
            var filesContent = new byte[file.ContentLength];

            using (var reader = new BinaryReader(file.InputStream, System.Text.Encoding.UTF8, false))
            {
                filesContent = reader.ReadBytes(file.ContentLength);
            }

            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");
            var path = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}\{file.FileName}";

            var result = _serviceClient.SaveFile(path, filesContent);

            if (result)
            {
                return Ok("File Uploaded and Saved successfully.");
            }

            return BadRequest("File Upload failed.");
        }

        [ResponseType(typeof(string))]
        [HttpDelete]
        public IHttpActionResult DeleteFile(string FullName)
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");

            var path = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}\{FullName}";

            var result = _serviceClient.DeleteFile(path);

            if (result)
            {
                return Ok($"Succesfully deleted {FullName}.");
            }

            return BadRequest($"Failed to delete {FullName}.");
        }

        [HttpGet]
        public IHttpActionResult EditFile(string FullName)
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");
            var path = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}\{FullName}";

            var file = _serviceClient.GetFile(path);

            var result = new HttpResponseMessage(HttpStatusCode.OK)
            {
                Content = new ByteArrayContent(file.Content)
            };
            result.Content.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("attachment")
            {
                FileName = FullName
            };
            result.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("application/octet-stream");

            var response = ResponseMessage(result);

            return response;
        }


        #endregion


        #region Helper Methods:

        /// <summary>
        /// Checks if the application is running locally or in ACP.
        /// </summary>
        /// <returns>True or False depending on the context.</returns>
        private bool IsApprenda()
        {
            try
            {
                return SessionContext.Instance != null;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public void SetPath(string Name)
        {
            var path = GetContext<string>("ResourceEditor-Position");

            try
            {
                if (Name.Equals(".."))
                {
                    path = path.Remove(path.LastIndexOf('\\'));
                }
                else
                {
                    path += $@"\{Name}";
                }

            }
            catch
            {
                path = "base";
            }

            SetContext("ResourceEditor-Position", path);
        }

        /// <summary>
        /// Gets the current value in the Session Cache by string key.
        /// </summary>
        /// <typeparam name="T">The object type to return as when getting from the cache.</typeparam>
        /// <param name="key">the string key index to use when looking up the object in the cache.</param>
        /// <returns></returns>
        public T GetContext<T>(string key)
        {
            if (IsApprenda())
            {
                // Get context from User Cache:
                return UserContext.Instance.Cache.Find<T>(key);
            }
            else
            {
                // Session, get context from asp session:
                return (T)HttpContext.Current.Session[key];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void SetContext<T>(string key, T value)
        {
            if (IsApprenda())
            {
                // Get context from User Cache:
                UserContext.Instance.Cache.Insert(key, value, TimeSpan.FromMinutes(20));
                //logger.Fatal($"Set the a value in user cache: { key } of type {value.GetType()}");
            }
            else
            {
                // Session, get context from asp session:
                HttpContext.Current.Session[key] = value;
            }
        }

        public void PurgeContext(string key)
        {
            if (IsApprenda())
            {
                // Get context from User Cache:
                UserContext.Instance.Cache.Evict(key);
                //logger.Fatal($"Set the a value in user cache: { key } of type {value.GetType()}");
            }
            else
            {
                // Session, get context from asp session:
                HttpContext.Current.Session.Remove(key);
            }
        }

        #endregion

    }
}
