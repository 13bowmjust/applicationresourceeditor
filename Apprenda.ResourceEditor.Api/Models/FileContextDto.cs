﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Apprenda.ResourceEditor.Api.ResourceEditorClient;

namespace Apprenda.ResourceEditor.Api.Models
{
    public class FileContextDto
    {

        public FileContext Context { get; set; }
        public string Position { get; set; }
        public string Application { get; set; }
        public string InteractionId { get; set; }

    }
}