﻿//$("a.ItemFile").click(function (e) {

//    e.preventDefault();
//    $.ajax({

//        url: $(this).attr("href"), // comma here instead of semicolon   
//        success: function () {
//            $(this).parent("div.FileItem").remove();
//        }

//    });

//});

$(".FileItem").click(function (e) {

    $("#contentFiles").find(".selected").removeClass("selected");

    $(this).addClass("selected");

});


$("#btnDownload").click(function (e) {
    var selected = $("#contentFiles").find(".selected");

    if (selected.length === 1) {
        var url = "../areditor--v1/Home/EditFile?FullName=" + $(selected).find(".item-text").text();

        window.location.href = url;
    }
});

$("#btnDelete").click(function (e) {
    var selected = $("#contentFiles").find(".selected");

    if (selected.length === 1) {
        $.ajax({
            url: "../areditor--v1/Home/DeleteFile?FullName=" + $(selected).find(".item-text").text(),
            success: function () {
                $("div").remove(".selected");
            }
        });
    }
});

$("#btnUpload").click(function (e) {

});

$("#btnSave").click(function (e) {

    $.ajax({
        url: "../areditor--v1/Home/SaveData",
        success: function (response) {
            window.location.href = response;
        },
        error: function (response) {
            alert("Saving data failed. Contact support with Context GUID.")
        }
    });

});

$("#btnCancel").click(function (e) {
    $.ajax({
        url: "../areditor--v1/Home/CancelData",
        success: function (response) {
            window.location.href = response;
        }
    });
});

$(window).unload(function () {
    $.ajax({
        url: "../areditor--v1/Home/CancelData",
        success: function (response) {

        }
    });
});
