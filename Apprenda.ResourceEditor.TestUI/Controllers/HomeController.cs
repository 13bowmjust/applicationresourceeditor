﻿using Apprenda.FileServices;
using Apprenda.ResourceEditor.TestUI.ResourceService;
using Apprenda.SaaSGrid;
using Apprenda.Services.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Apprenda.ResourceEditor.TestUI.Controllers
{
    public class HomeController : Controller
    {
        private FileServiceClient _serviceClient = new FileServiceClient();
        //private static ILogger logger = LogManager.Instance().GetLogger("Test Resource Editor");

        private bool IsApprenda()
        {
            var apprenda = false;

            try
            {
                apprenda = SessionContext.Instance != null;
            }
            catch (Exception ex)
            {
                apprenda = false;
            }

            return apprenda;
        }


        public ActionResult Index()
        {
            if (GetContext<InteractionContext>("ResourceEditor-Context") == null)
            {
                var context = new InteractionContext("bowman", "", "azpnetforge", "", "v11", "");
                _serviceClient.Initialize(context);
                SetContext("ResourceEditor-Context", context);
                SetContext("ResourceEditor-Position", "base");
            }

            GetData();

            return View();
        }

        public ActionResult GetNewPath(string Name)
        {
            var path = GetContext<string>("ResourceEditor-Position");

            try
            {
                if (Name.Equals(".."))
                {
                    path = path.Remove(path.LastIndexOf('\\'));
                }
                else
                {
                    path += $@"\{Name}";
                }

            }
            catch
            {
                path = "base";
            }
            SetContext("ResourceEditor-Position", path);
            return RedirectToAction("Index");
        }

        public void GetData()
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");

            var path = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}";

            var files = _serviceClient.GetFiles(path);

            ViewBag.FilesContext = files;
            ViewBag.Position = currentPosition;
            ViewBag.Application = context.ApplicaitonAlias + " - " + context.VersionAlias;
            ViewBag.InteractionId = context.InteractionId;
        }

        public JsonResult GetJsonData()
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");

            var path = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}";

            var files = _serviceClient.GetFiles(path);

            return Json(files);
        }

        public string CancelData()
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var path = $@"{context.ApplicationDetails}\{context.InteractionId}";

            _serviceClient.CancelEdits(path);

            PurgeContext("ResourceEditor-Context");
            PurgeContext("ResourceEditor-Position");

            return $"https://apps.apprenda.jbowman/developer/app/#/apps/dashboard/{context.ApplicaitonAlias}/{context.VersionAlias}";
        }

        public string SaveData()
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");

            _serviceClient.SaveEdits(context);

            PurgeContext("ResourceEditor-Context");
            PurgeContext("ResourceEditor-Position");

            return $"https://apps.apprenda.jbowman/developer/app/#/apps/dashboard/{context.ApplicaitonAlias}/{context.VersionAlias}";
        }

        public void DeleteFile(string FullName)
        {
            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");
            var path = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}\{FullName}";

            _serviceClient.DeleteFile(path);
        }

        public ActionResult EditFile(string FullName)
        {
            // bowman\azp\v11\base\api\filename
            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");
            var path = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}\{FullName}";

            var file = _serviceClient.GetFile(path);
            return File(file.Content, file.Extension, file.Name);
        }

        [HttpPost]
        public ActionResult UploadFile(HttpPostedFileBase file)
        {
            var filesContent = new byte[file.ContentLength];

            using (var reader = new BinaryReader(file.InputStream, System.Text.Encoding.UTF8, false))
            {
                filesContent = reader.ReadBytes(file.ContentLength);
            }

            var context = GetContext<InteractionContext>("ResourceEditor-Context");
            var currentPosition = GetContext<string>("ResourceEditor-Position");
            var path = $@"{context.ApplicationDetails}\{context.InteractionId}\{currentPosition}\{file.FileName}";

            _serviceClient.SaveFile(path, filesContent);

            return new EmptyResult();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public T GetContext<T>(string key)
        {
            if (IsApprenda())
            {
                // Get context from User Cache:
                return UserContext.Instance.Cache.Find<T>(key);
            }
            else
            {
                // Session, get context from asp session:
                return (T)Session[key];
            }
        }

        public void SetContext<T>(string key, T value)
        {
            if (IsApprenda())
            {
                // Get context from User Cache:
                UserContext.Instance.Cache.Insert(key, value, TimeSpan.FromMinutes(20));
                //logger.Fatal($"Set the a value in user cache: { key } of type {value.GetType()}");
            }
            else
            {
                // Session, get context from asp session:
                Session[key] = value;
            }
        }

        public void PurgeContext(string key)
        {
            if (IsApprenda())
            {
                // Get context from User Cache:
                UserContext.Instance.Cache.Evict(key);
                //logger.Fatal($"Set the a value in user cache: { key } of type {value.GetType()}");
            }
            else
            {
                // Session, get context from asp session:
                Session.Remove(key);
            }
        }
    }
}