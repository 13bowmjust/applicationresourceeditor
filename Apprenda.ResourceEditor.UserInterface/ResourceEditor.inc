﻿<div class="header">
    <h1>Resource Editor</h1>
</div>

<div class="mainContent">

    <div class="content-collection">

        <div class="text-info text-left">
            <h4><b>Application:</b>{{vm.filesContext.Application}}</h4>
            <h4><b>Archive:</b>{{vm.filesContext.Position}}</h4>
            <h4><b>Context:</b>{{vm.FilesContext.InteractionId}}</h4>
        </div>

        <div id="contentFiles">

            <div id="dirParent"
                 ng-click="vm.setPath('..')"
                 class="FileItem badge">
                <span class="glyphicon glyphicon-folder-open" />
                <p class="item-text">Parent Directory</p>
            </div>

            <div ng-repeat="x in vm.filesContext.Context.DirectoryStubs"
                 ng-click="vm.setPath(x.Name)"
                 class="FileItem badge">
                <span class="glyphicon glyphicon-folder-open glyphicon-resize-full" />
                <p class="item-text">{{x.Name}}</p>
            </div>

            <div ng-repeat="x in vm.filesContext.Context.FileStubs"
                 ng-click="vm.selectFile(x)"
                 ng-class="{selected : x === vm.selectedFile}"
                 class="FileItem badge">
                <span class="glyphicon glyphicon-file" />
                <p class="item-text">{{x.Name}}</p>
            </div>

        </div>
    </div>
</div>

<div class="action-bar">
    <div class="bar-actions">
        <div class="action">
            <button id="btnDownload" type="button" class="editor-btn action-btn">
                <span class="glyphicon glyphicon-cloud-download"></span>
                Edit
            </button>
        </div>
        <div class="action">
            <button id="btnUpload" type="button" class="editor-btn action-btn">
                <span class="glyphicon glyphicon-cloud-upload"></span>
                Upload
            </button>
        </div>
        <div class="action">
            <button id="btnDelete" type="button" class="editor-btn action-btn">
                <span class="glyphicon glyphicon-trash "></span>
                Delete
            </button>
        </div>
    </div>
</div>
