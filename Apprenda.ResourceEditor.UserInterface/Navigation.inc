﻿<section ng-controller="navigationController">
    <div class="navbar navbar-inverse sidenav navbarMaster">
        <div class="cap">
            <img src="~/Content/Images/apprenda.png" alt="Apprenda" />
        </div>
        <div>
            <div>
                <ul class="nav">
                    <li>
                        <button id="btnSave"
						type="button"
						class="editor-btn"
						ng-click="vm.saveData()">
                            <span class="glyphicon glyphicon-floppy-disk"></span>
                            Save
                        </button>
                    </li>
                    <li>
                        <button id="btnCancel"
						type="button"
						class="editor-btn"
						ng-click="vm.cancelData()">
                            <span class="glyphicon glyphicon-remove-circle"></span>
                            Cancel
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>