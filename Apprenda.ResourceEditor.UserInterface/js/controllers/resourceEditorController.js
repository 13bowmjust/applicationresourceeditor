﻿
var myApp = angular.module("appResourceEditor");


myApp.controller("resourceEditorController", ["apiService",

    function (apiService) {
        var vm = this;

        /* Declarations: */

        vm.selectedFile = null;
        vm.filesContext = null;


        /* VM Functions: */

        vm.setPath = function (path) {
            apiService.getData(path)
                .then(function successCallback(response) {
                    vm.filesContext = response.data;
                }, function errorCallback(response) {
                    console.log("Error getting the Files Context - " + response.statusText);
                });
        };

        vm.selectFile = function (file) {
            vm.selectedFile = file;
        };

        /* Initialize: */

        /* vm.setPath(""); */

    }

]);

