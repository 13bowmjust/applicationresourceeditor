﻿
var myApp = angular.module("appResourceEditor");


myApp.controller("navigationController", ["apiService",

    function (apiService) {
        var vm = this;

        /* Declarations: */

        /* VM Functions: */

        vm.saveData = function () {
            apiService.saveData()
                .then(function successCallback(response) {
                    console.log("Successfully Saved Archive data - " + response.statusText);
                },
                function errorCallback(response) {
                    console.log("Failed to Save Archive data - " + response.statusText);
                });
        };

        vm.cancelData = function () {

        };

        /* Initialize: */

    }

]);