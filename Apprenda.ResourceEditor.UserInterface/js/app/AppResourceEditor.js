﻿var myApp = angular.module("appResourceEditor", [
    "ngRoute"
]);

myApp.config(function ($routeProvider) {

    $routeProvider
        .when("/", {
            templateUrl: "ResourceEditor.inc",
            controller: "resourceEditorController",
            controllerAs: "vm"
        });

});


myApp.service("apiService", ["$http", "$q", function ($http, $q) {

    var baseUrl = "api/v1";

    this.getData = function (path) {
        $http.get(baseUrl + "/GetData?path=" + path);
    };

    this.saveData = function () {

    };

    this.cancelData = function () {

    };

    this.uploadFile = function (file) {

    };

    this.deleteFile = function (name) {

    };

    this.editFile = function (name) {

    };


}]);